The Go webgui client is a web server which is :
- a secured http server for the 'webgui' HTML/Javascript user interface.
- a websocket server for the 'wegui' HTML/Javascript user interface or any other client.
- a websocket client for all the assistant servers on the network.

So : 
- the html/js/css files are updated from the 'webgui' dedicated project during a production build.

The final binary can be used for the following usages :
- the embedded web user interface.
- some Android/iOS/whatever applications : this server will act as a gateway for remote usages.

