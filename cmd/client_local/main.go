package main

/* Brain cli client

This program is used to quickly test the brain from the command line.
*/

import (
	"bufio"
	"fmt"
	"gitlab.com/goassistant/assistant/pkg/brain"
	"gitlab.com/goassistant/assistant/pkg/logging"
	"os"
	"strings"
)

var Component string = "Command line client"

var log logging.LoggingContext
var assistantName string
var assistantFavoriteLanguage string

func main() {
	// Set logger

	log = logging.NewLogger(Component)
	log.Info(fmt.Sprintf("[ %s ]", Component))

	// Load the brain
	brain.Load()
	// Get the assistant informations
	assistantName = brain.AssistantName()
	assistantFavoriteLanguage = brain.AssistantFavoriteLanguage()

	// Init the stdin
	reader := bufio.NewReader(os.Stdin)

	// Eternal loop to read input and process it
	for {
		fmt.Print("\nyou > ")
		// read input
		text, _ := reader.ReadString('\n')
		fmt.Print("\n")
		// convert CRLF to LF
		text = strings.Replace(text, "\n", "", -1)

		// process the input text and display the response
		request := brain.BrainExchange{
			Type: "dialog",
			Text: text,
		}
		brain.ProcessJsonExchange(request, cbResponse, nil)

	}
}

func cbResponse(response brain.BrainExchange, unused interface{}) {
	if response.Type == "dialog" && response.Ack == false {
		fmt.Println("\n" + assistantName + " (" + assistantFavoriteLanguage + ")" + " > " + response.Text + "\n")
	}
}
