package main

import (
	"gitlab.com/goassistant/assistant/pkg/version"
	"html/template"
	"net/http"
)

// httpRoot()
// Display the / page
func httpRoot(w http.ResponseWriter, req *http.Request) {
	type data struct {
		Component string
		Version   string
	}
	tmpl := template.Must(template.ParseFiles("templates/root.html"))
	tmpl.Execute(w, data{Component: Component, Version: version.Version})
}
