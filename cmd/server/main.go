package main

import (
	"flag"
	"fmt"
	"gitlab.com/goassistant/assistant/pkg/brain"
	"gitlab.com/goassistant/assistant/pkg/discovery"
	"gitlab.com/goassistant/assistant/pkg/logging"
	"gitlab.com/goassistant/assistant/pkg/security"
	"gitlab.com/goassistant/assistant/pkg/tools"
	"net"
	"net/http"
	"os"
)

var Component string = "Assistant Server"
var Roles []string = []string{"server", "event.consumer"}
var Name string = ""
var Address string = ""
var Hostname string = ""
var defaultHttpsPort int = 0 // setting it to zero allow to make the port dynamic on startup
var sslCrtFile string = "ssl_server.crt"
var sslKeyFile string = "ssl_server.key"

var log logging.LoggingContext

// https://tutorialedge.net/golang/go-websocket-tutorial/

func main() {
	// Parameters
	var portPtr = flag.Int("port", defaultHttpsPort, "Listening port")
	flag.Parse()

	// Set logger
	log = logging.NewLogger(Component)

	// Check if the ssl required files exist.
	weNeedToGenerateCertificateAndKeyForHttps := false
	if _, err := os.Stat(sslCrtFile); os.IsNotExist(err) {
		weNeedToGenerateCertificateAndKeyForHttps = true
	}
	if _, err := os.Stat(sslKeyFile); os.IsNotExist(err) {
		weNeedToGenerateCertificateAndKeyForHttps = true
	}

	// If not, create them
	if weNeedToGenerateCertificateAndKeyForHttps {
		security.GenerateCertificateAndKey(sslCrtFile, sslKeyFile)
	}

	// Load the brain
	brain.Load()

	// Get server hostname and name (role@hostname)
	Hostname, Name = tools.GetHostnameAndName(Roles)

	// Get server ip adressess
	ip := tools.GetMainIP()
	port := *portPtr

	// If needed, find a dynamic Port
	var listener net.Listener
	var err error
	if port == 0 {
		listener, err = net.Listen("tcp", ":0")
		if err != nil {
			log.Fatal(fmt.Sprintf("%v", err))
		}
		port = listener.Addr().(*net.TCPAddr).Port
		log.Info(fmt.Sprintf("The port is dynamically affected. Port is '%d'", port))
	} else {
		listener, err = net.Listen("tcp", fmt.Sprintf(":%d", port))
	}

	// Launch discovery
	Address = fmt.Sprintf("%s:%d", ip, port) // also used by urlInfo.go
	discoveryData := discovery.BuildServerInformations(Address, Roles)
	go discovery.ShowMeToTheLAN(discoveryData)

	// Routing
	http.HandleFunc("/", httpRoot)
	http.HandleFunc("/info", httpInfo)
	http.HandleFunc("/ws/brain", wsBrainEndpoint) // webSocket

	// Execute the ssl_server
	log.Info(fmt.Sprintf("Starting HTTPS server on port %d", port))
	//err := http.ListenAndServeTLS(fmt.Sprintf(":%d", port), sslCrtFile, sslKeyFile, logRequest(http.DefaultServeMux))
	err = http.ServeTLS(listener, log.HttpRequest(http.DefaultServeMux), sslCrtFile, sslKeyFile)
	if err != nil {
		log.Fatal(fmt.Sprintf("ListenAndServe: %v", err))
	}

	// Nothing will be executed here...
}
