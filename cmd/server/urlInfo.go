package main

import (
	"encoding/json"
	"gitlab.com/goassistant/assistant/pkg/version"
	"net/http"
)

type Info struct {
	Component string   `json:"component"`
	Version   string   `json:"version"`
	Roles     []string `json:"roles"`
	Name      string   `json:"name"`
	Address   string   `json:"address"`
}

// httpInfo()
// Return the /info json
func httpInfo(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	data := Info{Component, version.Version, Roles, Name, Address}
	json.NewEncoder(w).Encode(data)
}
