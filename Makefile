###################################
# 
# Sample Makefiles : https://github.com/helm/helm/blob/master/Makefile
#
# TODO LIST
#
# - sudo sysctl -w vm.max_map_count=262144 # for elasticsearch in docker
#   OR :
#   	root@ambre:/media/data/gobutler/assistant/elk# cat /proc/sys/vm/max_map_count
#	65530
#	root@ambre:/media/data/gobutler/assistant/elk# ls -l /proc/sys/vm/max_map_count
#	-rw-r--r-- 1 root root 0 déc.   4 10:17 /proc/sys/vm/max_map_count
#	root@ambre:/media/data/gobutler/assistant/elk# echo 262144 >/proc/sys/vm/max_map_count
#	root@ambre:/media/data/gobutler/assistant/elk# cat /proc/sys/vm/max_map_count
#	262144
#
# - sudo for docker commands ?
# - check for npm
# - check go version : 1.12 at least
# - for all go binaries, handle the cross compilation
#
# Note about cross compilation
# ----------------------------
#
# Two different commands are used depending if we do cross compilation or not :
# - 'go install' when no cross compilation
#   This one is used because it maintains a cache and so compilation is quicker
# - 'go build' with cross compilation
#   As 'go install' is not fine with cross compilation in our case (not compliant with a $GOBIN target for example),
#   we use 'go build' instead.
#
# To NOT run cross compilation, just do : make
#
# To run cross compilation, just do :  env GOARCH=arm64 GOOS=linux make server
# A dedicted folder will be created for each component in dist : ./dist/server_linux_arm64 for example.
#
###################################

MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
MKFILE_DIR := $(dir $(MKFILE_PATH))

# Go parameters

# You should have this kind of folder: 
# workspace/
#   src/
#     gitlab.com/
#       goassistant/
#         assistant/
# The GOPATH will refer to the workspace
GOPATH=$(MKFILE_DIR)/../../../../
export GOPATH

PACKAGE="gitlab.com/goassistant/assistant"

# Set version in code during compilation from git 
# Example : 0.2-1-g87bea26
VERSION=$(shell git describe --tags)
GOLDFLAGS=-X $(PACKAGE)/pkg/version.Version=$(VERSION)
GOFLAGS=-ldflags "$(GOLDFLAGS)"

# Global Go variables
GOCMD=go
GODEBUG=
GOINSTALL=$(GOCMD) install $(GOFLAGS)
GOBUILD=$(GOCMD) build $(GOFLAGS)
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
GOMOD=$(GOCMD) mod
BIN_DIR=$(MKFILE_DIR)/dist

# Set extensio for binaries
EXTENSION_linux=
EXTENSION_windows=.exe
EXTENSION := $(EXTENSION_$(GOOS))

# Check some tools are present or not
DOCKER_BIN := $(shell command -v docker 2> /dev/null)
DOCKER_COMPOSE_BIN := $(shell command -v docker-compose 2> /dev/null)

# Docker labelling
DOCKER_LABEL=Assistant

# As 'webgui' is a rule and a directory we need to add this to avoid "make 'webgui' is up to date"
.PHONY: webgui

# Clean, get dependencies, test and build all
all: clean deps test build_webgui build

# Clean, get dependencies, test and build all except web gui
no_gui: clean deps test build

### Make entries

# Help
help:
	@awk '/^#/{c=substr($$0,3);next}c&&/^[[:alpha:]][[:alnum:]_-]+:/{print substr($$1,1,index($$1,":")),c}1{c=0}' $(MAKEFILE_LIST) | column -s: -t

# Build main components - server, client local, client websocket, vue.js webgui, webgui server
build: 
	@echo "############################################"
	@echo "# Build"
	@echo "############################################"

	# ~~~~ server ~~~~
	$(call build_server)

	# ~~~~ client local ~~~~
	$(call build_client_local)
	
	# ~~~~ client websocket ~~~~
	$(call build_client_websocket)

	# ~~~~ client web gui ~~~~
	$(call build_client_webgui)
	
	@echo "End with success :)"

build_webgui: 
	@echo "############################################"
	@echo "# Build Web GUI" 
	@echo "############################################"

	# ~~~~ GUI webgui ~~~~
	# npm part
	$(call build_webgui)

	@echo "End with success :)"

# Build the server
server: 
	@echo "############################################"
	@echo "# Build server"
	@echo "############################################"
	$(call build_server)

	@echo "End with success :)"

# Build the webgui server (but not the vue.js webgui part)
webgui: 
	@echo "############################################"
	@echo "# Build webgui (without building the GUI)"
	@echo "############################################"
	$(call build_client_webgui)

	@echo "End with success :)"


# Execute the tests
test: 
	@echo "############################################"
	@echo "# Test"
	@echo "############################################"
	$(GOTEST) -v ./...


### Functions

# Configuration for components with Brain embedded in it
BRAIN_CONFIG_SAMPLE=./configs/brain-config.yml.sample
BRAIN_CONFIG_TARGET=brain-config.yml
PACKAGES_SRC=./packages


# Client local (brain included)
CLIENT_LOCAL_BIN=client_local$(EXTENSION)
ifndef GOARCH
CLIENT_LOCAL_DIR=$(BIN_DIR)/client_local
CLIENT_LOCAL_CMD=GOBIN=$(CLIENT_LOCAL_DIR)/ $(GOINSTALL)
else
CLIENT_LOCAL_DIR=$(BIN_DIR)/client_local_$(GOOS)_$(GOARCH)
CLIENT_LOCAL_CMD=$(GOBUILD) -o $(CLIENT_LOCAL_DIR)/$(CLIENT_LOCAL_BIN)
endif
define build_client_local
	mkdir -p $(CLIENT_LOCAL_DIR)

	mkdir -p $(CLIENT_LOCAL_DIR)/packages
	cp -Rp $(PACKAGES_SRC)/* $(CLIENT_LOCAL_DIR)/packages/

	cp -Rp $(BRAIN_CONFIG_SAMPLE) $(CLIENT_LOCAL_DIR)/
	[ ! -f $(CLIENT_LOCAL_DIR)/$(BRAIN_CONFIG_TARGET) ] && cp -Rp $(BRAIN_CONFIG_SAMPLE) $(CLIENT_LOCAL_DIR)/$(BRAIN_CONFIG_TARGET) || true

	$(CLIENT_LOCAL_CMD) $(GODEBUG) gitlab.com/goassistant/assistant/cmd/client_local

	ls -ltrh $(CLIENT_LOCAL_DIR)/$(CLIENT_LOCAL_BIN)
endef

# Client : websocket sample
CLIENT_WEBSOCKET_BIN=client_websocket$(EXTENSION)
ifndef GOARCH
CLIENT_WEBSOCKET_DIR=$(BIN_DIR)/client_websocket
CLIENT_WEBSOCKET_CMD=GOBIN=$(CLIENT_WEBSOCKET_DIR)/ $(GOINSTALL)
else
CLIENT_WEBSOCKET_DIR=$(BIN_DIR)/client_websocket_$(GOOS)_$(GOARCH)
CLIENT_WEBSOCKET_CMD=$(GOBUILD) -o $(CLIENT_WEBSOCKET_DIR)/$(CLIENT_WEBSOCKET_BIN)
endif
define build_client_websocket
	mkdir -p $(CLIENT_WEBSOCKET_DIR)
	$(CLIENT_WEBSOCKET_CMD) $(GODEBUG) gitlab.com/goassistant/assistant/cmd/client_websocket
	ls -ltrh $(CLIENT_WEBSOCKET_DIR)/$(CLIENT_WEBSOCKET_BIN)
endef

# GUI : webgui
define build_webgui
	( cd ./webgui ; ./build.sh )
	ls -ltrh ./webgui/dist/index.html
endef

# Client : webgui
CLIENT_WEBGUI_TEMPLATES=./cmd/webguiserver/templates/* 
CLIENT_WEBGUI_VUE_FILES=./webgui/dist/* 
CLIENT_WEBGUI_BIN=webguiserver$(EXTENSION)
ifndef GOARCH
CLIENT_WEBGUI_DIR=$(BIN_DIR)/webgui
CLIENT_WEBGUI_CMD=GOBIN=$(CLIENT_WEBGUI_DIR)/ $(GOINSTALL)
else
CLIENT_WEBGUI_DIR=$(BIN_DIR)/webgui_$(GOOS)_$(GOARCH)
CLIENT_WEBGUI_CMD=$(GOBUILD) -o $(CLIENT_WEBGUI_DIR)/$(CLIENT_WEBGUI_BIN)
endif
CLIENT_WEBGUI_DOCKER_IMAGE=assistant-webgui
define build_client_webgui
	mkdir -p $(CLIENT_WEBGUI_DIR)

	mkdir -p $(CLIENT_WEBGUI_DIR)/templates
	cp -Rp $(CLIENT_WEBGUI_TEMPLATES) $(CLIENT_WEBGUI_DIR)/templates/

	# Grab files from webgui project
	mkdir -p $(CLIENT_WEBGUI_DIR)/web
	cp -Rp $(CLIENT_WEBGUI_VUE_FILES) $(CLIENT_WEBGUI_DIR)/web/

	$(CLIENT_WEBGUI_CMD) $(GODEBUG) gitlab.com/goassistant/assistant/cmd/webguiserver
	ls -ltrh $(CLIENT_WEBGUI_DIR)/$(CLIENT_WEBGUI_BIN)
endef

# Assistant Server (brain included)
SERVER_TEMPLATES=./cmd/server/templates/* 
SERVER_BIN=server$(EXTENSION)
ifndef GOARCH
SERVER_DIR=$(BIN_DIR)/server
SERVER_CMD=GOBIN=$(SERVER_DIR)/ $(GOINSTALL)
else
SERVER_DIR=$(BIN_DIR)/server_$(GOOS)_$(GOARCH)
SERVER_CMD=go build -o $(SERVER_DIR)/$(SERVER_BIN)
endif
SERVER_DOCKER_IMAGE=assistant-server
define build_server
	mkdir -p $(SERVER_DIR)

	mkdir -p $(SERVER_DIR)/templates
	cp -Rp $(SERVER_TEMPLATES) $(SERVER_DIR)/templates/

	mkdir -p $(SERVER_DIR)/packages
	cp -Rp $(PACKAGES_SRC)/* $(SERVER_DIR)/packages/

	cp -Rp $(BRAIN_CONFIG_SAMPLE) $(SERVER_DIR)/
	[ ! -f $(SERVER_DIR)/$(BRAIN_CONFIG_TARGET) ] && cp -Rp $(BRAIN_CONFIG_SAMPLE) $(SERVER_DIR)/$(BRAIN_CONFIG_TARGET) || true

	$(SERVER_CMD) $(GODEBUG) gitlab.com/goassistant/assistant/cmd/server
	ls -ltrh $(SERVER_DIR)/$(SERVER_BIN)
endef



# Clean all builded files
clean: 
	@echo "############################################"
	@echo "# Clean"
	@echo "############################################"
	$(GOCLEAN) $(PACKAGE)

	# Clean client : local
	rm -f $(CLIENT_LOCAL_DIR)/*.log
	rm -f $(CLIENT_LOCAL_DIR)/*sample
	rm -f $(CLIENT_LOCAL_DIR)/$(CLIENT_LOCAL_BIN)*
	rm -Rf $(CLIENT_LOCAL_DIR)/packages

	# Clean client : websocket sample
	rm -f $(CLIENT_WEBSOCKET_DIR)/*.log
	rm -f $(CLIENT_WEBSOCKET_DIR)/$(CLIENT_WEBSOCKET_BIN)*

	# Clean GUI : web gui
	# TODO
	
	# Clean client : web gui
	rm -f $(CLIENT_LOCAL_DIR)/*.log
	rm -f $(CLIENT_LOCAL_DIR)/$(CLIENT_WEBGUI_BIN)*
	rm -f $(CLIENT_WEBGUI_DIR)/ssl_webgui*
	rm -Rf $(CLIENT_WEBGUI_DIR)/templates
	rm -Rf $(CLIENT_WEBGUI_DIR)/static
	rm -Rf $(CLIENT_WEBGUI_DIR)/web

	# Clean server 
	rm -f $(SERVER_DIR)/*.log
	rm -f $(SERVER_DIR)/*sample
	# TODO : clean yml ?
	rm -f $(SERVER_DIR)/$(SERVER_BIN)*
	rm -Rf $(SERVER_DIR)/packages
	rm -f $(SERVER_DIR)/ssl_server*
	rm -Rf $(SERVER_DIR)/templates

	# TODO : clean xml files (which ones ??? don't remember)
	# TODO : don't clean the output files!
	
	# if docker binary exists
	# TODO : do as root
	# If Docker, clean docker images and containers
	# clean stopped containers
	#sudo docker container prune -f --filter "label=project=$(DOCKER_LABEL)"  # The Dockerfile sets the label
	# clean images
	#sudo docker image prune -f -a --filter "label=project=$(DOCKER_LABEL)"





# Download dependencies
deps:
	@echo "############################################"
	@echo "# Deps"
	@echo "############################################"

	GO111MODULE=on $(GOMOD) vendor

# Build the docker images
docker_build:
ifndef DOCKER_BIN
	$(error "Docker is not installed. Please install it")
endif
	@echo "############################################"
	@echo "# Docker : build images"
	@echo "# - 2 servers"
	@echo "# - 1 webgui"
	@echo "############################################"

	### Prepare the Dockerfiles
	# webgui
	@sed -e 's|{DIR}|.|' \
		-e 's|{LABEL}|$(DOCKER_LABEL)|' \
		-e 's|{BINARY}|$(CLIENT_WEBGUI_BIN)|' \
		-e 's|{EXPOSE_PORT}|EXPOSE 8443|' \
		Dockerfile.tpl > $(CLIENT_WEBGUI_DIR)/Dockerfile
	
	# server 'lambda' : the 2 servers will use the same docker image
	@sed -e 's|{DIR}|.|' \
		-e 's|{LABEL}|$(DOCKER_LABEL)|' \
		-e 's|{BINARY}|$(SERVER_BIN)|' \
		-e 's|{EXPOSE_PORT}||' \
		Dockerfile.tpl > $(SERVER_DIR)/Dockerfile

        ### Prepare dedicated configuration files for the both servers described in docker-compose.yml
	# Each server/brain should have a different name from the other one in the case we use docker in the network mode 'host'.
	# In this mode, all servers will have the same ip and the default behavious is to assume that there is one single server/brain
	# per host.
	sed "s/subname.*/subname: \"1\"/" $(SERVER_DIR)/$(BRAIN_CONFIG_TARGET) > $(SERVER_DIR)/brain-config-server1.yml
	sed "s/subname.*/subname: \"2\"/" $(SERVER_DIR)/$(BRAIN_CONFIG_TARGET) > $(SERVER_DIR)/brain-config-server2.yml

	### Build the docker images
	# webgui
	(cd $(CLIENT_WEBGUI_DIR)/ ; docker build -t $(CLIENT_WEBGUI_DOCKER_IMAGE) .)
	
	# server
	(cd $(SERVER_DIR)/ ; docker build -t $(SERVER_DOCKER_IMAGE) .)

	# Run the docker images
	@echo "Sample commands to run the images : "
	#docker run -p 8443:8443 $(CLIENT_WEBGUI_DOCKER_IMAGE) 
	#docker run $(SERVER_DOCKER_IMAGE) 

	@echo "End with success :)"

# Run the docker images
docker_run:
ifndef DOCKER_COMPOSE_BIN
	$(error "Docker compose is not installed. Please install it")
endif
	@echo "############################################"
	@echo "# Docker : run images"
	@echo "############################################"

	docker-compose up
