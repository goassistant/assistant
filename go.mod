module gitlab.com/goassistant/assistant

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/uuid v1.1.1 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/gorilla/websocket v1.4.1
	github.com/lithammer/shortuuid v3.0.0+incompatible
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/schollz/peerdiscovery v1.4.1 // indirect
	github.com/sirupsen/logrus v1.4.2
	gopkg.in/yaml.v3 v3.0.0-20191120175047-4206685974f2
)
