'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  //ASSISTANT_ADDRESS: '"127.0.0.1:8443"',
  ASSISTANT_ADDRESS: '"192.168.1.50:8443"',
})
