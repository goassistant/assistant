Building
========

To build the binaries for the current platform, just run :

```
$ make
```

When finished, the binaries, default configuration files and dockerfiles are in the dist/ folder.

Docker
======

Build images
------------

To test the project with Docker, you can build the docker images.

First, be sure to have build the binaries (and the Dockerfiles) with : 

```
$ make
```

Then you can build the docker images. They will be populated with the builded binaries (this way the docker images are quick to build).

```
$ sudo make docker_build
```

The following images are build : 

* assistant-server
* assistant-webgui

Run images manually
-------------------

TODO

Run a demo
----------

To run a demo with one webgui and two servers, just run:

```
$ sudo docker-compose up
```

You will be able to read the log files in /tmp/log.

To run a demo with one webgui, two servers and a Elastic based log stack, just run:

```
$ sudo docker-compose -f docker-compose.yml -f elk/docker-compose.yml up
```




