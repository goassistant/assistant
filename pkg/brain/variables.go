package brain

import (
	"errors"
	"fmt"
	"gitlab.com/goassistant/assistant/pkg/cache"
	"regexp"
	"strings"
)

// SetVariable()
// Set a variable in the cache.
// The item in the cache will be : ''variable:name'
func SetVariable(name string, value string) {
	name = fmt.Sprintf("variable:%s", name)
	cache.Set(name, value)
}

// GetVariable()
// Set a variable in the cache.
// The item in the cache will be : ''variable:name'
// We assume here that a variable can only be a string !
func GetVariable(name string) string {
	name = fmt.Sprintf("variable:%s", name)
	result := cache.Get(name)
	resultString := result.(string)
	return resultString
}

// GetAllVariablesAsText()
// Return as text all the variables :
//  aa=bb
//  cc=dd
//  ...
func GetAllVariablesAsText() string {
	// cache.GetAllItems()
	data := ""
	for k, v := range cache.GetAllItems().(map[string]string) {
		data = fmt.Sprintf("%s\n%s = %v", data, k, v)
	}
	if data == "" {
		data = "No variables set." // TODO : i18n
	}
	return data
}

// ApplyVariablesToString()
// Replace all {{ variableName }} items of a string by the variables values
func ApplyVariablesToString(data string) (string, error) {
	response := data

	// We use the function from packages.go to get the variables names
	_, varNames := processVariables(strings.ToLower(data), data)

	for _, v := range varNames {
		reText := fmt.Sprintf("{{ *%s *}}", v)
		re, err := regexp.Compile(reText)
		if err != nil {
			msg := fmt.Sprintf("Error while attempting to replace a variable in response : unable to compile the regular expression '%s'", reText)
			log.Error(msg)
			return msg, errors.New(msg)
		}
		response = re.ReplaceAllString(response, GetVariable(v))
	}
	return response, nil
}
