package brain

type BrainExchange struct {
	// Security //////////////////////////////////////////////////
	// TODO : for now, this is just used on WebGui side to check incoming messages from the user
	Token string `json:"token"` // the authentication Token

	// Technical /////////////////////////////////////////////////
	Type string `json:"type"` // the type of message : system, dialog
	Ack  bool   `json:"ack"`  // is this an ACK message or not ? If ACK message, all other informations (except type and requestId and ConversationId) are ingored

	// Below is for type="system" ////////////////////////////////
	// TODO : complete
	Os            string `json:"os"`            // the server operating system
	Arch          string `json:"arch"`          // the server cpu architecture
	Hostname      string `json:"hostname"`      // the server hostname
	AssistantName string `json:"assistantName"` // the name of the assistant

	// Below is for type="dialog" ////////////////////////////////
	Text           string `json:"text"`           // the user request text
	Media          string `json:"media"`          // the source media : voice, text
	FromAssistant  bool   `json:"fromAssistant"`  // is the message coming from an assistant or a user ? If true, an assistant
	Name           string `json:"name"`           // the user name (of the name or of the assistant)
	ConversationId string `json:"conversationId"` // the conversion uuid
	// A conversation could be one request/response or several request/response

	RequestId     string `json:"requestId"`     // the request uuid : 1 request = 1 response => the same id for both
	ValidResponse bool   `json:"validResponse"` // if set to true, this is a valid response from the server :
	// true : it means the server was able to process the Request
	// false : the server was not able to process the request and send some text like "I don't understand".
	//         In this case, if the first server which respond has not a valid response, we should skip it.
	//         We should only use this response if there is no other server which could reply

	Html  string             `json:"html"`  // additionnal (and optionnal) html data to be displayed
	Video BrainExchangeVideo `json:"video"` // additionnal video content

	// Below is optionnal technical informations
	ClientId string `json:"clienttId"` // This is can be used by the client to help the internal routing of the messages
	// This is used for example bu the webgui : each websocket connection on user side has its own client id

	// TODO ?
	// - add language response to help choosing the voice when media = voice

}

type BrainExchangeVideo struct {
	Name     string `json:"name"`     // Name of the video. Only for user information
	Url      string `json:"url"`      // Url of the vidéo. Should be http://... or https://...
	Format   string `json:"format"`   // The video format : mjpeg, youtube, ...
	Login    string `json:"login"`    // Optionnal : login (for mjpeg cameras for example)
	Password string `json:"password"` // Optionnal : password (for mjpeg cameras for example)

}
