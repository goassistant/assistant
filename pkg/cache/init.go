package cache

/*
  This package provides a way to store data in a persistent cache

  Local cache uses https://github.com/patrickmn/go-cache
*/

// TODO : handle concurrency
// TODO : handle (if enabled in configuration) a redis cache instead of the local one

import (
	"fmt"
	gocache "github.com/patrickmn/go-cache"
	"gitlab.com/goassistant/assistant/pkg/logging"
	"time"
)

var Component string = "Cache"
var log logging.LoggingContext

var localCache *gocache.Cache

// init()
func init() {
	// Set logger
	log = logging.NewLogger(Component)

	// Init the local cache
	// Create a cache with a default expiration time of 5 minutes, and which
	// purges expired items every 10 minutes
	// Notice that here we use no expiration time (for now), so we set big values

	// TODO : try loading from the cache persistent file it it exists
	localCache = gocache.New(1440*time.Minute, 2880*time.Minute)
}

// Set()
func Set(key string, value interface{}) {
	// TODO : add persistence (think of mutex!)
	log.Info(fmt.Sprintf("Cache set : '%s' = '%+v'", key, value))
	localCache.Set(key, value, gocache.NoExpiration)
	log.Debug(fmt.Sprintf("Cache set : '%s'. Success", key))
}

// Get()
func Get(key string) interface{} {
	log.Debug(fmt.Sprintf("Cache get : '%s'.", key))
	value, found := localCache.Get(key)
	if found {
		log.Info(fmt.Sprintf("Cache get : '%s' = '%+v'", key, value))
		return value
	}
	log.Info(fmt.Sprintf("Cache get : '%s' = <NOTFOUND>", key))
	return ""
}

// GetAllItems()
// Return all cache content in a map[string]string
func GetAllItems() interface{} {
	log.Debug(fmt.Sprintf("Cache get all items"))
	items := localCache.Items()
	result := make(map[string]string)
	for k, _ := range items {
		tmp, _ := localCache.Get(k) // not v to avoid casting
		result[k] = tmp.(string)
	}
	log.Info(fmt.Sprintf("Cache get all items: '%+v'", result))
	return result
}
