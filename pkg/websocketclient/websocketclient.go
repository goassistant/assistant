package websocketclient

/* Assistant web socket client library
   This package is made to be included in any websocket client :
   it will handle the discovery of hosts and exchange with them
*/

import (
	"crypto/tls"
	"fmt"
	"github.com/gorilla/websocket"
	"gitlab.com/goassistant/assistant/pkg/brain"
	"gitlab.com/goassistant/assistant/pkg/discovery"
	"gitlab.com/goassistant/assistant/pkg/logging"
	"gitlab.com/goassistant/assistant/pkg/tools"
	"net"
	"net/url"
	"os/user"
	"time"
)

var Component string = "WebSocket client lib"

var log logging.LoggingContext

var endpoint string = "/ws/brain"

// Requests handling /////////////////////////////////

// Prepare channel to send data to all goroutines that handle websockets
// This channel will be used to send the request to all the servers.
// The reply will be processed by the callback passed to initWebSocket()
var requestsChannels = make(map[string](chan brain.BrainExchange))

// The map of "requests informations"
// When a request is emeitted, we add {N, 0} to the map (N=number of servers).
// When a valid response is received, we remove the request from the map
// When an invalid response is received, we do receivedResponses++
// If receivedResponses == numberOfServers : we process the response
// Else, we skip the response
// This is done to keep only the last invalid response ("I don't understand" or something like that)
type requestsInformations struct {
	invalidReceivedResponses int    // the number of invalid responses received
	server                   string // the id (address) of the server which will fully handle the request id
}

// TODO : later on, this may be updated due to :
// TODO : - the capacity of the server brain to tell if it can handle the request before sending the response (for long actions)
// The map entry is a requestId
var requestsIdWithNoResponses = make(map[string]*requestsInformations)

// Servers list //////////////////////////////////////

// Prepare channel to handle newly discovered servers (and only servers)
// This channel will be used to populate a list of already discovered servers to
// avoid creating several websocket connections to the same server
var newServerChannel = make(chan string)

// The list of the discovered servers
var serversList []string // the list of the servers which will be writen only from waitForNewServer()

// The list of the 'once' connected (over websocket) servers
// This list can be different of the 'serversList' as we keep an history of disconected servers
type ViewedServer struct {
	Online  bool   `json:"online"`  // true is the server is online.
	Name    string `json:"name"`    // the nice looking name of the server. This is used as unique ID in the web interface
	Address string `json:"address"` // the address of the server
}

type ViewedServersExchange struct {
	Type    string         `json:"type"`    // the type of message : system, dialog, here : 'servers'
	Servers []ViewedServer `json:"servers"` // the list of viewed servers
}

var viewedServersList = ViewedServersExchange{
	Type:    "servers",
	Servers: make([]ViewedServer, 0),
}
var viewedServersListVersion int = 0

// Peers list ////////////////////////////////////////

// A channel to handle the newly discovered 'things' which could be a server, a processor or anything else
// The difference between a peer and a server is :
// - a server is a peer with roles = ['server', ...]
// - some additionnal informations are given over websockets by the servers.
// - a peer could be a 'server', a processor : 'motion.detection', 'face.recognition', ...
// - a 'server' status (online/offline) is set with : peer discovery for getting online, websocket closed for getting offline.
//   The online/offline status about a server is important for a user : it helps to see the whole list of brains and if they are available
//   or not when speaking to the assistant.
//   A peer status (online/offline) is not so usefull. We just need to get the list of the online peers to be able to configure them from the UI
type Peer struct {
	Address      string    `json:"address"`      // the address of the Peer
	Roles        []string  `json:"roles"`        // the roles of the Peer
	Name         string    `json:"name"`         // the 'raw' name of the Peer : the name given by UDP discovery.
	LastTimeSeen time.Time `json:"lastTimeSeen"` // when the peer was seen on UDP discovery the last time (for cleanup)
}

var newPeerChannel = make(chan Peer)

// The list of the discovered Peers
// The list will be cleaned with a goroutine
type PeersExchange struct {
	Type  string `json:"type"`  // the type of message : system, dialog, here : 'peers'
	Peers []Peer `json:"peers"` // the list of viewed peers
}

var peersList = PeersExchange{
	Type:  "peers",
	Peers: make([]Peer, 0),
}
var peersListVersion int = 0

// Other things //////////////////////////////////////

// The user name
var ComputerUsername string

// Response handler defined by the client
var responseHandler func(brain.BrainExchange)

//////////////////////////////////////////////////////

func init() {
	// Set logger
	log = logging.NewLogger(Component)

	// Get user informations (logname)
	user, err := user.Current()
	if err != nil {
		// If we are not able to get the user informations, never mind!
		// We will set a default user name.
		log.Warn(fmt.Sprintf("Unable to get the user logname. This is not so important. The error is : %v", err))
		ComputerUsername = "User"
	}
	ComputerUsername = user.Username
	log.Info(fmt.Sprintf("Your name is : %s", ComputerUsername))

	// Peers discovery
	// 1. start waiting for new servers messages on related channel
	go waitForNewServer()
	go waitForNewPeer()
	// 2. start listening the network to find new servers
	go discovery.SearchForPeersOnLAN(onPeerDiscoverd)
	// 3. cleanup of peers
	// Notice that servers are cleaned when a websocket connection is closed/lost
	// The list of peers is cleaned by checking the last seen time
	go cleanupPeers()

}

// SetResponseHandler()
// Set the response handler for the caller
func SetResponseHandler(cb func(data brain.BrainExchange)) {
	responseHandler = cb
}

// initWebSocket()
// Init a websocket connection with a server
func initWebSocket(addr string, cbReceived func(addr string, v interface{})) {
	// Preprare ws connection
	//interrupt := make(chan os.Signal, 1)
	//signal.Notify(interrupt, os.Interrupt)

	u := url.URL{Scheme: "wss", Host: addr, Path: endpoint}
	log.Info(fmt.Sprintf("[ server %s ] connecting to %s", addr, u.String()))

	// Custom dialer to allow connecting to self signed certificates
	config := &tls.Config{
		InsecureSkipVerify: true,
	}
	dialer := websocket.Dialer{
		TLSClientConfig: config,
	}

	// Connect
	// Do several attempts as the remote server may not be ready on websocket side just after the discovery process
	connOk := false
	maxConnTry := 12
	numConnTry := 0
	connDelayMs := 5000 * time.Millisecond
	var c *websocket.Conn
	var err error
	for connOk == false && numConnTry <= maxConnTry {
		c, _, err = dialer.Dial(u.String(), nil)
		if err != nil {
			log.Warn(fmt.Sprintf("[ server %s ] dial attempt %d/%d : %v", addr, numConnTry, maxConnTry, err))
			numConnTry = numConnTry + 1
			time.Sleep(connDelayMs)
		} else {
			connOk = true
		}

	}
	if connOk == false {
		log.Error(fmt.Sprintf("[ server %s ] Too much attempts to connect to the server. Canceling!", addr))
		return
	}

	defer c.Close()

	done := make(chan struct{})

	// Make the channel
	requestsChannels[addr] = make(chan brain.BrainExchange)

	// Read input messages forever in a thread (goroutine)
	go func() {
		defer close(done)
		for {
			data := brain.BrainExchange{}
			err := c.ReadJSON(&data)
			if err != nil {
				// Error with websocket : the client or server had disconnect.
				log.Error(fmt.Sprintf("[ server %s ] Error while reading on websocket: %v", addr, err))
				log.Info(fmt.Sprintf("Removing the server '%s' from the list of servers...", addr))
				// Remove the channel
				delete(requestsChannels, addr)

				// Remove the server from the list of servers
				i := tools.SliceIndexOf(serversList, addr)
				serversList = tools.SlicePopIndex(serversList, i)

				// Set offline the server from the list of viewed servers
				log.Info(fmt.Sprintf("Set offline the server '%s' from the list of viewed servers...", addr))
				for idx, val := range viewedServersList.Servers {
					if val.Address == addr {
						viewedServersList.Servers[idx].Online = false
						viewedServersListVersion += 1
					}
				}

				// TODO : do we get out of all the root function initWebSocket ?
				return

			}
			cbReceived(addr, data)
		}

	}()

	// Eternal loop on channel for user requests
	go func() {
		for {
			request := <-requestsChannels[addr]
			log.Debug(fmt.Sprintf("[ server %s ] Writing JSON : %+v", addr, request))
			requestsIdWithNoResponses[request.RequestId] = &requestsInformations{
				invalidReceivedResponses: 0,
				server:                   "",
			}
			err := c.WriteJSON(request)
			if err != nil {
				log.Error(fmt.Sprintf("[ server %s ] writeJson: %v", addr, err))
			}
		}
	}()

	// Websocket related channels
	for {
		select {
		case <-done:
			return
		}
	}

}

// onWebsocketMessageReceived()
// Process a received message on websocket.
// Input :
// - addr : the address of the server from which we get the data (used)
// - data : the data receied
func onWebsocketMessageReceived(addr string, v interface{}) {
	log.Info(fmt.Sprintf("[ server %s ] onWebsocketMessageReceived : %+v", addr, v))
	log.Debug(fmt.Sprintf("requestsIdWithNoResponses=%+v", requestsIdWithNoResponses))

	var data brain.BrainExchange

	// Cast the data
	switch v.(type) {
	case brain.BrainExchange:
		// appropriate type, continue on :)
		data = v.(brain.BrainExchange)
	default:
		log.Warn(fmt.Sprintf("[ server %s ] onWebsocketMessageReceived > unexpected message type received. Type='%T'. Message='%+v'", addr, data, data))
		return
	}
	log.Debug(fmt.Sprintf("- servers = %+v", requestsIdWithNoResponses[data.RequestId]))

	if data.Type == "system" {
		// Process the 'system' messages
		serverName := fmt.Sprintf("%s@%s", data.AssistantName, data.Hostname)
		found := false
		for idx, val := range viewedServersList.Servers {
			// TODO : DEL if val.Address == addr {
			if val.Name == serverName {
				// The server was already viewed. Set is status to true
				viewedServersList.Servers[idx].Online = true
				viewedServersListVersion += 1
				found = true
			}
		}
		if found == false {
			viewedServersList.Servers = append(viewedServersList.Servers, ViewedServer{
				Online:  true,
				Name:    fmt.Sprintf("%s@%s", data.AssistantName, data.Hostname),
				Address: addr,
			})
			viewedServersListVersion += 1
		}
		log.Info(fmt.Sprintf("Update of the viewed servers list : %+v", viewedServersList))

	} else if data.Type == "dialog" {
		// Process the 'dialog' messages

		// Check if the request id has already be processed
		// If not, set as processed, then process the response

		// When a request is emeitted, we add {N, 0} to the map (N=number of servers).
		// When a valid response is received, we remove the request from the map
		// When an invalid response is received, we do receivedResponses++
		// If receivedResponses == numberOfServers : we process the response
		// Else, we skip the response
		// This is done to keep only the last invalid response ("I don't understand" or something like that)

		if _, ok := requestsIdWithNoResponses[data.RequestId]; !ok {
			// If the requestId is no more present in the map....
			// well, it should not happen in a user=>assistant dialog.
			// Or it means that it is the next step of a response which taked a lot of time bacause the map
			// requestsIdWithNoResponses has been cleaned.
			// So it means that this is a dialog issued by the assistant.
			// We will just log a Debug message and process the "text sent from the brain"

			// TODO : Log as Warn for now, move in Debug later
			log.Warn(fmt.Sprintf("[ server %s  ] onWebsocketMessageReceived > a message is received from this requestId '%s'. This requestId is not present in the map. This culd be related to a dialog initiated by the assistant. Or a 'next step' of a request which took a long long time....", addr, data.RequestId))
		}

		// Check if the response is valid
		if data.ValidResponse {
			// The response is valid, we set the server value to the server address.
			// So the next responses from servers other than this one will be skipped.
			// And the next reponses from this specific server for this request will be processed.

			if requestsIdWithNoResponses[data.RequestId].server == "" {
				// If server == nil, it means that this is the first valid response.
				// We will so set this server as the one which will handle the next responses from this requestId.
				// Reminder : example of a multi response request :
				// you> sleep for 10s
				// assistant> ok, I will sleep
				// ... 10 s ...
				// assistant> my sleep is finished
				requestsIdWithNoResponses[data.RequestId].server = addr
			} else {
				// The server field is already set... Let's see if the response come from the appropriate server.
				// The other ones will be skipped.
				if requestsIdWithNoResponses[data.RequestId].server == addr {
					log.Debug(fmt.Sprintf("[ server %s  ] onWebsocketMessageReceived > a message is received from the good server for requestId '%s'. Processing it.", addr, data.RequestId))
				} else {
					log.Debug(fmt.Sprintf("[ server %s  ] onWebsocketMessageReceived > a message is received from a server which is not the first that replied for requestId '%s'. Skipping it.", addr, data.RequestId))
					return
				}
			}

		} else {
			// The response is not valid.
			// In case there are several servers trying to process the request, we should process only
			// the last invalid response.

			// Increment the number of invalid received reponses
			requestsIdWithNoResponses[data.RequestId].invalidReceivedResponses += 1

			log.Debug(fmt.Sprintf("serversList=%+v", serversList))
			log.Debug(fmt.Sprintf("len(serversList)=%d", len(serversList)))
			if len(serversList) == requestsIdWithNoResponses[data.RequestId].invalidReceivedResponses {
				// If we have reached the last response
				log.Debug(fmt.Sprintf("[ server %s  ] onWebsocketMessageReceived > the last invalid response message is received from a server that replied for requestId '%s'. Processing it.", addr, data.RequestId))

			} else {
				// This is not the last response... we skip it!
				log.Debug(fmt.Sprintf("[ server %s  ] onWebsocketMessageReceived > an invalid response message is received from a server which is not the last that replied for requestId '%s'. Skipping it.", addr, data.RequestId))
				return
			}
		}

		// Process the response
		// TODO : replace by a channel ????
		responseHandler(data)
	}

}

// WaitForOnePeerDiscovered()
func WaitForOnePeerDiscovered() {

	// First wait 6 second to get a clear output
	// 6 seconds because the discovery time is 5s, so we should have found all local serverAddress
	// and be fully ready
	// This is something not needed on other clients.
	time.Sleep(discovery.DiscoveryDelaySeconds * time.Second)

	// Check if there is at least one server discovered. If not, do a loop to wait
	idx := 0
	for len(serversList) == 0 {
		if idx%5 == 0 { // wait 5 seconds before displaying some warning
			log.Warn("No server discovered! Waiting for discovering a server...")
		}
		time.Sleep(1 * time.Second)
		idx = idx + 1
	}

}

func onPeerDiscoverd(src *net.UDPAddr, data string) {
	// Activate this log only for debugging the discovery !
	// log.Debug(fmt.Sprintf("Server discovery : received from %v : %s", src, data))

	serverAddress := discovery.GetPeerAddress(data)
	roles := discovery.GetPeerRoles(data)
	name := discovery.GetPeerName(data)

	//if roles[0] == "server" {
	if tools.SliceContainsString(roles, "server") {
		// In a server case, the name will be overriden by data return over websocket. So we don't need it right here.

		// log.Debug(fmt.Sprintf("Discovered : roles=%v, address=%s [valid]", roles, serverAddress))
		addr := serverAddress
		// Write to the channel to handle the discovered server
		// log.Debug("CHANNEL : send addr")
		newServerChannel <- addr
	} else {
		// log.Debug(fmt.Sprintf("Discovered : roles=%v, address=%s [skipped : we search only for roles=['server', ...] ", roles, serverAddress))
	}

	// for all kind of peer
	// log.Debug("CHANNEL : send peer")
	newPeerChannel <- Peer{Address: serverAddress,
		Roles:        roles,
		Name:         name,
		LastTimeSeen: time.Now(),
	}
}

func waitForNewServer() {
	for {
		// Wait for a new discovery message from channel
		addr := <-newServerChannel
		//log.Debug("CHANNEL : received addr")

		// Check if the server is already in the list or not
		if !tools.SliceContainsString(serversList, addr) {
			log.Info(fmt.Sprintf("New server discovered : %s.", addr))
			serversList = append(serversList, addr)
			log.Info(fmt.Sprintf("List of discovered servers updated : %v.", serversList))
			go initWebSocket(addr, onWebsocketMessageReceived)
		}
	}
}

func waitForNewPeer() {
	for {
		// Wait for a new discovery message from channel
		peer := <-newPeerChannel
		//log.Debug("CHANNEL : received peer")

		// Check if the server is already in the list or not
		// - check only server addr
		// - add a last time seen field
		// - add a function to cleanup if last time seen > 2 * DiscoveryDelaySeconds
		found := false
		for idx, p := range peersList.Peers {
			if p.Address == peer.Address {
				found = true
				peersList.Peers[idx].LastTimeSeen = time.Now()
			}
		}
		if !found {
			peersList.Peers = append(peersList.Peers,
				Peer{Address: peer.Address, // yeah, basically we could have made a simple copy...
					Name:         peer.Name,
					Roles:        peer.Roles,
					LastTimeSeen: peer.LastTimeSeen,
				})
			log.Info(fmt.Sprintf("List of discovered peers updated : %v.", peersList))
			peersListVersion += 1
		}

	}
}

// cleanupPeers()
// Clean the peers list based on the last time seen field
func cleanupPeers() {
	for {
		timeNow := time.Now()
		cleanup := false
		for idx, peer := range peersList.Peers {
			if timeNow.Sub(peer.LastTimeSeen).Seconds() > discovery.DiscoveryDelaySeconds*2 {
				// TODO : fix here and then clean the debug line
				/*
					panic: runtime error: slice bounds out of range

					goroutine 23 [running]:
					assistant/websocketclient.cleanupPeers()
					        /media/data/gobutler/src/assistant/websocketclient/websocketclient.go:497 +0x4b5
							created by assistant/websocketclient.init.0
							        /media/data/gobutler/src/assistant/websocketclient/websocketclient.go:149 +0x28a
				*/
				log.Debug(fmt.Sprintf("XXXX idx=%v, len=%v", idx, len(peersList.Peers)))
				peersList.Peers = append(peersList.Peers[:idx], peersList.Peers[idx+1:]...)
				cleanup = true
			}
		}
		if cleanup {
			log.Info(fmt.Sprintf("List of peers cleaned. New list : %+v", peersList.Peers))
			peersListVersion += 1
		}
		time.Sleep(discovery.DiscoveryDelaySeconds * 2 * time.Second)
	}
}

// PublishRequest()
// Publish a request to all the discovered servers
func PublishRequest(data brain.BrainExchange) {
	// Write to the servers channels
	for k, _ := range requestsChannels {
		requestsChannels[k] <- data
	}

}

// GetViewedServers()
// Return the list of viewed servers : address, nice name and online/offline
func GetViewedServers() (ViewedServersExchange, int) {
	return viewedServersList, viewedServersListVersion
}

// GetPeers()
// Return the list of peers
func GetPeers() (PeersExchange, int) {
	return peersList, peersListVersion
}
