package tools

import (
	"fmt"
	"net"
	"strings"
)

// GetAllMyIPs()
// Return all ip of the server except loopback
func GetAllMyIPs() []string {
	var result []string
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		log.Error(fmt.Sprintf("GetAllMyIPs : %v", err))
	}

	for _, a := range addrs {
		if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				if !strings.HasPrefix(ipnet.IP.String(), "169.") {
					log.Debug(fmt.Sprintf("GetAllMyIPs : ip of server : '%s'", ipnet.IP.String()))
					result = append(result, ipnet.IP.String())
				}
			}
		}
	}
	return result
}

// GetMainIP()
// Return the main non loopback ip of the server
func GetMainIP() string {
	// TODO : the first is the best ? or not ?
	result := GetAllMyIPs()[0]
	log.Debug(fmt.Sprintf("GetMainIP : main ip is '%s'", result))
	return result
}
