package tools

import (
	"fmt"
	"github.com/lithammer/shortuuid"
	"gitlab.com/goassistant/assistant/pkg/logging"
	"net/url"
	"os"
	"strings"
)

var Component string = "Tools"
var log logging.LoggingContext

func init() {
	// Set logger
	log = logging.NewLogger(Component)
}

func GetShortUUID() string {
	u := shortuuid.New()
	return u
}

func GetHostnameAndName(roles []string) (string, string) {
	// Get server hostname
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "unknown"
	}

	// Set default peer name
	name := fmt.Sprintf("%s@%s", roles[0], hostname)
	return hostname, name
}

func IsValidUrl(toTest string) bool {
	// Check if the input is an url or not.
	// Return true if this is an url

	// test the prefix
	if !strings.HasPrefix(toTest, "http") {
		return false
	}

	// use some builtin... which does not check the prefix
	_, err := url.ParseRequestURI(toTest)
	if err != nil {
		return false

	} else {
		return true

	}

}
