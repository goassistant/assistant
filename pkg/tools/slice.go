package tools

import ()

/* Slice related helpers
 */

// SliceContainsString()
// Check if the given slice contrain the given string
// Return true if it contains the string
func SliceContainsString(slice []string, item string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}

	}
	_, ok := set[item]
	return ok
}

// SliceIndexOf()
// Return the index of an element in data.
// Return -1 if not found
func SliceIndexOf(data []string, element string) int {
	for k, v := range data {
		if element == v {
			return k
		}
	}
	return -1 //not found.

}

// SlicePopIndex()
// Pop the element at index i from the slice/array
func SlicePopIndex(a []string, i int) []string {
	a[i] = a[len(a)-1] // Copy last element to index i.
	a[len(a)-1] = ""   // Erase last element (write zero value).
	b := a[:len(a)-1]  // Truncate slice.
	return b
}
