package security

import (
	"gitlab.com/goassistant/assistant/pkg/logging"
)

var Component string = "Security"
var log logging.LoggingContext

var AssistantSecret = "secret"

func init() {
	// Set logger
	log = logging.NewLogger(Component)
}
