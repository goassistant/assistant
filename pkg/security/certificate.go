package security

/* This package file is used to build some SSL certificate and key files.

Source : https://gist.github.com/samuel/8b500ddd3f6118d052b5e6bc16bc4c09
Updated for my needs :)
*/

import (
	"bytes"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	"os"
	"time"
)

// Certificate details
var Organization string = "__ Golang Assistant __"
var CountryCode string = "FR"

func publicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}

func pemBlockForKey(priv interface{}) *pem.Block {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k)}
	case *ecdsa.PrivateKey:
		b, err := x509.MarshalECPrivateKey(k)
		if err != nil {
			log.Fatal(fmt.Sprintf("Unable to marshal ECDSA private key: %v", err))
		}
		return &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}
	default:
		return nil
	}
}

// GenerateCertificateAndKey()
// Create the 2 files needed for our https server :
// - the certificate
// - the key
func GenerateCertificateAndKey(crtFile string, keyFile string) {
	log.Info("Generating certificates for the HTTPS server...")
	priv, err := rsa.GenerateKey(rand.Reader, 2048)
	//priv, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		log.Fatal(fmt.Sprintf("%v", err))
	}
	template := x509.Certificate{
		SerialNumber: big.NewInt(1),
		Subject: pkix.Name{
			Organization: []string{Organization},
			Country:      []string{CountryCode},
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().Add(time.Hour * 24 * 180),

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	/*
	   hosts := strings.Split(*host, ",")
	   for _, h := range hosts {
	   	if ip := net.ParseIP(h); ip != nil {
	   		template.IPAddresses = append(template.IPAddresses, ip)
	   	} else {
	   		template.DNSNames = append(template.DNSNames, h)
	   	}
	   }
	   if *isCA {
	   	template.IsCA = true
	   	template.KeyUsage |= x509.KeyUsageCertSign
	   }
	*/

	derBytes, err := x509.CreateCertificate(rand.Reader, &template, &template, publicKey(priv), priv)
	if err != nil {
		log.Fatal(fmt.Sprintf("Failed to create certificate: %v", err))
	}
	out := &bytes.Buffer{}
	pem.Encode(out, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	// fmt.Println(out.String())

	// Create the CERTIFICATE
	log.Info(fmt.Sprintf("- creating certificate : %v", crtFile))
	f, err := os.Create(crtFile)
	if err != nil {
		log.Fatal(fmt.Sprintf("Error while creating file '%s' : %v ", crtFile, err))

	}
	fmt.Fprint(f, out.String())
	if err != nil {
		f.Close()
		log.Fatal(fmt.Sprintf("Error while writing in file '%s' : %v ", crtFile, err))

	}
	err = f.Close()
	out.Reset()

	// Create the KEY
	log.Info(fmt.Sprintf("- creating key : %s", keyFile))
	pem.Encode(out, pemBlockForKey(priv))
	// fmt.Println(out.String())
	f, err = os.Create(keyFile)
	if err != nil {
		log.Fatal(fmt.Sprintf("Error while creating file '%s' : %v ", keyFile, err))

	}
	fmt.Fprint(f, out.String())
	if err != nil {
		f.Close()
		log.Fatal(fmt.Sprintf("Error while writing in file '%s' : %v ", keyFile, err))

	}
	err = f.Close()

	log.Info("Certificates successfully generated!")
}
