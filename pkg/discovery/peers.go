package discovery

import (
	"encoding/json"
	"fmt"
	"gitlab.com/goassistant/assistant/pkg/logging"
	"net"
	"os"
	"time"
)

// Inspired from https://gist.github.com/fiorix/9664255

const (
	DiscoverySrvAddr         = "224.1.1.1:9999"
	DiscoveryMaxDatagramSize = 8192
	DiscoveryDelaySeconds    = 5
)

var Component string = "Peers"
var log logging.LoggingContext

// Data in peer discovery

type JsonStruct struct {
	Address string   `json:"address"`
	Roles   []string `json:"roles"`
	Name    string   `json:"name"`
}

func init() {
	// Set logger
	log = logging.NewLogger(Component)
}

// testDiscovery()
// Execute the threads related to UDP discovery on the LAN.
// This function is mainly used for test
/*
func testDiscovery() {
	go ShowMeToTheLAN("hello world!\n")
	go SearchForPeersOnLAN(msgHandler)
}
*/

// showMeToTheWorld()
// Write the server informations on UDP to show the LAN we are here!
func ShowMeToTheLAN(message string) {
	log.Info(fmt.Sprintf("Start ShowMeToTheLAN : we will emit a 'alive' message each %d seconds", DiscoveryDelaySeconds))
	log.Info(fmt.Sprintf("The alive message will be : %s", message))
	addr, err := net.ResolveUDPAddr("udp", DiscoverySrvAddr)
	if err != nil {
		log.Fatal(fmt.Sprintf("ShowMeToTheLAN : Error while initiating UDP dialog : %v", err))
	}
	c, err2 := net.DialUDP("udp", nil, addr)
	if err2 != nil {
		log.Fatal(fmt.Sprintf("ShowMeToTheLAN : Error while initiating UDP dialog (2): %v", err2))
	}
	for {
		msg := []byte(message)
		// log.Debug(fmt.Sprintf("ShowMeToTheLAN : send '%v'", string(msg)))
		c.Write(msg)
		time.Sleep(DiscoveryDelaySeconds * time.Second)
	}
}

// msgHandler()
// Just an handler.
func msgHandler(src *net.UDPAddr, data string) {
	log.Debug(fmt.Sprintf("read from %v : %s", src, data))
}

// searchForPeers()
// Listen to UDP in an eternal loop to discover other hosts
func SearchForPeersOnLAN(h func(*net.UDPAddr, string)) {
	log.Info("Initializing searching for peers on LAN")
	addr, err := net.ResolveUDPAddr("udp", DiscoverySrvAddr)
	if err != nil {
		log.Fatal(fmt.Sprintf("Discovery : Error while initiating UDP dialog : %v", err))

	}
	l, err2 := net.ListenMulticastUDP("udp", nil, addr)
	if err2 != nil {
		log.Fatal(fmt.Sprintf("Discovery : Error while initiating UDP dialog (2): %v", err2))
	}
	l.SetReadBuffer(DiscoveryMaxDatagramSize)
	log.Info("Start searching for peers on LAN")
	for {
		b := make([]byte, DiscoveryMaxDatagramSize)
		n, src, err := l.ReadFromUDP(b)
		if err != nil {
			log.Fatal(fmt.Sprintf("ReadFromUDP failed: %v", err))
		}
		// TODO : comment
		log.Debug("UDP data...")
		h(src, string(b[:n]))
	}
}

// For servers

// BuildServerInformations()
// From a json input (as string), return the address
// address = 192.168.1.50:8443
// roles = ["server", "other", ...]
// returns {"address" : "192.168.1.50:8443", "role": "server"}
func BuildServerInformations(address string, roles []string) string {
	// TODO : use json.Marshal to build the json string in a proper way
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "unknown"
	}
	name := fmt.Sprintf("%s@%s", roles[0], hostname)
	jsonData := &JsonStruct{address, roles, name}
	data, err2 := json.Marshal(jsonData)
	if err2 != nil {
		log.Fatal(fmt.Sprintf("Error while building server informations for discovery: %s", err2))
	}
	//return fmt.Sprintf("{\"address\" : \"%s\", \"role\": \"%s\", \"name\": \"%s\"}", address, role[0], name)
	data_str := string(data) + "\n\r"
	return data_str
}

// For clients

// GetPeerAddress()
// From a json input (as string), return the address
// {"address" : "192.168.1.50:8443", "role": "xxx", "name:" : "yyy"} => return "192.168.1.50:8443"
func GetPeerAddress(jsonStr string) string {
	var jsonData JsonStruct
	err := json.Unmarshal([]byte(jsonStr), &jsonData)
	if err != nil {
		log.Error(fmt.Sprintf("GetServerAddress : unable to parse the json: %v", err))
	}
	return jsonData.Address
}

// GetPeerRoles()
// From a json input (as string), return the role
// {"address" : "192.168.1.50:8443", "role": "xxx", "name:" : "yyy"} => return "xxx""
func GetPeerRoles(jsonStr string) []string {
	var jsonData JsonStruct
	err := json.Unmarshal([]byte(jsonStr), &jsonData)
	if err != nil {
		log.Error(fmt.Sprintf("GetServerAddress : unable to parse the json: %v", err))
	}
	return jsonData.Roles
}

// GetPeerName()
// From a json input (as string), return the role
// {"address" : "192.168.1.50:8443", "role": "xxx", "name:" : "yyy"} => return "yyy""
func GetPeerName(jsonStr string) string {
	var jsonData JsonStruct
	err := json.Unmarshal([]byte(jsonStr), &jsonData)
	if err != nil {
		log.Error(fmt.Sprintf("GetServerAddress : unable to parse the json: %v", err))
	}
	return jsonData.Name
}
